package uk.co.jmsoft.gradlejava9runtimeimageplugin;

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Test

public class CustomRuntimeImagePluginTest {

    @Test
    public void canAddPluginToProject() {
        Project project = ProjectBuilder.builder().build()
        project.apply( plugin: CustomRuntimeImagePlugin)
    }
}
