package uk.co.jmsoft.gradlejava9runtimeimageplugin;

import org.gradle.api.Project
import org.junit.Test
import static org.junit.Assert.*
import org.gradle.testfixtures.ProjectBuilder

public class LinkTaskTest {
    @Test
    public void canAddTaskToProject() {
        Project project = ProjectBuilder.builder().build()
        def task = project.task('link', type: LinkTask)
        assertTrue(task instanceof LinkTask)
    }
}
