package uk.co.jmsoft.gradlejava9runtimeimageplugin

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

import java.util.regex.Matcher


class LaunchTask extends DefaultTask{

    /**
     * Commandline options to be passed to the jvm when the application launches
     */
    @Input
    String vmOptions = ""

    @TaskAction
    void launch(){
        project.exec {
            String os = System.getProperty("os.name").toLowerCase()

            if(os.indexOf("win") >= 0) {
                def batFile = new File("${project.link.outputDir.path}/bin/", "launch.bat")
                String batContent = batFile.text
                batFile.text = batContent.replaceFirst('JLINK_VM_OPTIONS=', Matcher.quoteReplacement("JLINK_VM_OPTIONS=$vmOptions"))
                logger.info "launch task: Platform detected as: $os. executing launch.bat"
                commandLine "${project.link.outputDir.path}/bin/launch.bat"
            }
            else
            {
                def bashFile = new File("${project.link.outputDir.path}/bin/", "launch")
                String bashContent = bashFile.text
                bashFile.text = bashContent.replaceFirst('JLINK_VM_OPTIONS=', Matcher.quoteReplacement('JLINK_VM_OPTIONS=$vmOptions'))
                logger.info "launch task: Platform detected as: $os. executing launch bash script"
                commandLine "${project.link.outputDir.path}/bin/launch"
            }
        }
    }





}
