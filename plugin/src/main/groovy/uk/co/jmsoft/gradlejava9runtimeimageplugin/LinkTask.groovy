/*
 * Copyright 2019 John Marshall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package uk.co.jmsoft.gradlejava9runtimeimageplugin

import org.gradle.api.DefaultTask
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import org.gradle.api.InvalidUserDataException

public class LinkTask extends DefaultTask {

    final String defaultModulePath

    /**
     * Sets whether a launcher script is created in the runtime image.
     */
    @Input
    boolean createLauncher = true

    /**
     * The directory where the runtime image will be created.
     */
    @OutputDirectory
    File outputDir

    /**
     * The modules that will be added to the module path. These can be jmod or JAR modules.
     */
    @InputFiles
    FileCollection moduleSources

    @Internal
    boolean moduleSourcesSetByUser = false

    String getModuleSourcesPath() {
        if (moduleSourcesSetByUser) {
            //Jlink seems to need module paths separated by ; for some reason?
            return moduleSources.join(";")
        }
        if(getJavaHome() == null) {
            throw new InvalidUserDataException("link failed to generate default module sources because Java home was not set by user")
        }
        // return defaults
        return "${getJavaHome()}jmods/;" +
                "${project.files(project.configurations.compileClasspath).asPath};" +
                "${project.jar.destinationDirectory.get().asFile.path}"
    }

    void setModuleSources(FileCollection moduleSources) {
        this.moduleSources = moduleSources
        moduleSourcesSetByUser = true
    }
    /**
     * The location of the JDK that will be used to create the runtime image.
     */
    @Input
    String javaHome = "NULL"
    /**
     * Returns the path of the JDK to use for the link task if it has been set explicitly by the user.
     * In order of precedence this will be:
     * <ol>
     *     <li>link.userHome set in the users build script</li>
     *     <li>org.gradle.java.home=<java home> set in a gradle.properties file</li>
     *     <li>The environment variable JAVA_HOME</li>
     * </ol>
     * Otherwise returns null
     * @return A string representing the path to the JDK used for the link task or null if no path has been
     * explicitly set by the user.
     */
    String getJavaHome() {
        //If user has set value in their build script use this value
        if (!javaHome.equals("NULL")) {
            return javaHome
        }
        // Next if user has set org.gradle.java.home in gradle.properties use that value
        def props = new Properties()
        project.file("gradle.properties").withInputStream { props.load(it) }
        String tempHome = props.getProperty("org.gradle.java.home")
        //Next try JAVA_HOME environment variable
        if (tempHome == null) {
            tempHome = System.getenv("JAVA_HOME")
        }
        //Otherwise return null
        return tempHome
    }

    @TaskAction
    void link() {
        outputDir.deleteDir()

        if (!moduleSourcesSetByUser) {
            logger.info "link task: moduleSources were not set by user, using defaults: ${getModuleSourcesPath()}"
        }

        project.exec {
            def command = getCommand()
            logger.info "link task: Executing command: $command"
            commandLine command
        }

    }

    /**
     * Generates the path to the jlink executable based on the user set java home path (<i>"<java home>/bin/jlink"</i>).<br>
     * If the user has not set a java home path has been set returns "jlink"
     * @return
     */
    String getJlinkPath() {
        String tempHome = getJavaHome()
        if (tempHome != null) {
            return "${tempHome}bin/jlink"
        }
        //If no javahome set try jlink on system path
        return "jlink"
    }

    List<String> getCommand() {
        List<String> list = new ArrayList<>()
        list.add(getJlinkPath())
        list.addAll(['--module-path', getModuleSourcesPath()])
        list.addAll(['--add-modules', project.moduleName])
        list.addAll(['--output', outputDir.path])
        if (createLauncher) {
            list.addAll(['--launcher', "launch=$project.moduleName/$project.mainClassName"])
        }
        return list
    }

}
