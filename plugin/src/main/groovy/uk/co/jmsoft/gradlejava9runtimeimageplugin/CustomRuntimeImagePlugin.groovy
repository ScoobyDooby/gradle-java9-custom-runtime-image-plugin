/*
 * Copyright 2019 John Marshall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package uk.co.jmsoft.gradlejava9runtimeimageplugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.bundling.Jar


class CustomRuntimeImagePlugin implements Plugin<Project> {
    final String groupName = "runtimeimage"
    @Override
    void apply(Project project) {
        project.apply plugin: "java"
        project.apply plugin: "application"
        project.logger.info "Creating moduleName extension"
        project.ext.moduleName = ""

        project.task("link", type: LinkTask) {
            description = "Create a launch-able custom runtime image."
            group = groupName
            dependsOn "jar"

            outputDir = new File(project.getBuildDir().path + "/runtimeimage")
            moduleSources = project.files()
            moduleSourcesSetByUser = false

        }

        project.task("launch", type: LaunchTask){
            description = "Launch the linked project using the custom runtime image."
            group = groupName
            dependsOn "link"
        }

    }
}
