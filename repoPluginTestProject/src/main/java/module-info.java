module uk.co.jmsoft.java9runtimeimage {
    requires javafx.controls;
    opens uk.co.jmsoft.java9runtimeimage to javafx.graphics;
}